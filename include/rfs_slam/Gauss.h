/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 16, 2013
 *     Author: Vladimir Petrik <vladko.petrik@gmail.com>
 */

#ifndef GAUSS_H_
#define GAUSS_H_

#include <vector>
#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <math.h>

template<class state, class covariance>
class Gauss {
public:
    typedef std::vector<state, Eigen::aligned_allocator<state> > vec_state;
    typedef std::vector<covariance, Eigen::aligned_allocator<covariance> > vec_covariance;
public:
    Gauss();
    Gauss(const state& mu, const covariance& cov, const double& w = 1.0);
    virtual ~Gauss();

    /** \brief Compute probability density function times weight */
    double pdf(const state& x) const;

    /** \brief Compute Eigen values and eigen vectors */
    void eigen_value_vectors(std::vector<double>& eigen_values,
            vec_state& eigen_vectors) const;

    /** \brief Compute probability density function of gaussian N(mu,cov) at state x */
    static double GaussianPdf(const state& x, const state& mu,
            const covariance& cov);

    /** \brief Get weight */
    double get_w() const;

    /** \brief Get mean value */
    state get_mu() const;

    /** \brief Get covariance */
    covariance get_cov() const;

    /** \brief Set weight */
    void set_w(double w);

    /** \brief Set mean value */
    void set_mu(const state& mu);

    /** \brief Set covariance */
    void set_cov(const covariance& cov);

    /** \brief Get mahalanobis distance squared (x-m)'*inv(cov)*(x-m)
     *  \details Weighted euclidean distance where weight is determined
     *  using covariance. */
    double mahalanobis_distance_squared(const state& x) const;

private:
    double w;
    state mu;
    covariance cov;
};

template<class state, class covariance>
inline Gauss<state, covariance>::Gauss() :
        w(1.0) {
    mu = state::Zero();
    cov = covariance::Identity();
}

template<class state, class covariance>
inline Gauss<state, covariance>::Gauss(const state& mu, const covariance& cov,
        const double& w) {
    this->mu = mu;
    this->cov = cov;
    this->w = w;
}

template<class state, class covariance>
inline Gauss<state, covariance>::~Gauss() {
}

template<class state, class covariance>
inline double Gauss<state, covariance>::pdf(const state& x) const {
    return GaussianPdf(x, mu, cov) * w;
}

template<class state, class covariance>
inline void Gauss<state, covariance>::eigen_value_vectors(
        std::vector<double>& eigen_values, vec_state& eigen_vectors) const {
    state eig_real = cov.eigenvalues().real();
    for (int i = 0; i < mu.rows(); ++i) {
        eigen_values.push_back((double) eig_real(i));
    }
    Eigen::EigenSolver<covariance> es(cov);
    for (int i = 0; i < mu.rows(); ++i) {
        state v = es.eigenvectors().col(i).real();
        eigen_vectors.push_back(v);
    }
}

template<class state, class covariance>
inline double Gauss<state, covariance>::GaussianPdf(const state& x, const state& mu,
        const covariance& cov) {
    static const double GAUSS_PI_CONST = pow(2 * M_PI, -mu.rows() / 2.0);

    double det = cov.determinant();
    covariance icov = cov.inverse();

    state v;
    v = x - mu;
    double exponent = ((double) (v.transpose() * (icov * v))) * (-0.5);

    if (isnan(exponent))
        return 0.0;
    double nu = GAUSS_PI_CONST / sqrt(det);
    double ret = exp(exponent) * nu;
    return ret;
}

template<class state, class covariance>
inline double Gauss<state, covariance>::get_w() const {
    return w;
}

template<class state, class covariance>
inline state Gauss<state, covariance>::get_mu() const {
    return mu;
}

template<class state, class covariance>
inline covariance Gauss<state, covariance>::get_cov() const {
    return cov;
}

template<class state, class covariance>
inline double Gauss<state, covariance>::mahalanobis_distance_squared(
        const state& x) const {
    state v = x - mu;
    return v.transpose() * cov.inverse() * v;
}

template<class state, class covariance>
inline void Gauss<state, covariance>::set_w(double w) {
    this->w = w;
}

template<class state, class covariance>
inline void Gauss<state, covariance>::set_mu(const state& mu) {
    this->mu = mu;
}

template<class state, class covariance>
inline void Gauss<state, covariance>::set_cov(const covariance& cov) {
    this->cov = cov;
}

#endif /* GAUSS_H_ */
