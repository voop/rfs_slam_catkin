/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 18, 2013
 *     Author: Vladimir Petrik <vladko.petrik@gmail.com>
 *    Details: Base class for filtering algorithm, PHD,CPHD,... will derive this methods.
 */

#ifndef FILTERBASE_H_
#define FILTERBASE_H_

#include <rfs_slam/GMM.h>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

/** \brief Base class for Filtering algorithm like PHD, CPHD
 *  \param observation - Eigen::VectorN
 *  \param covariance - Eigen::MatrixN
 *  \param robot_state - Eigen::AffineMd.
 *  \details Important: M <= N, i.e. the dimension of the robot state space must by less of equal
 *           to dimension of the observation state space
 */
template<class observation, class covariance, class robot_state>
class FilterBase {
    BOOST_STATIC_ASSERT_MSG(int(observation::RowsAtCompileTime) >= int(robot_state::MatrixType::RowsAtCompileTime)-1
            , "Robot State Space Dimension must be smaller or equal to the Observation State Space Dimension");
    BOOST_STATIC_ASSERT_MSG(int(observation::RowsAtCompileTime) == int(covariance::ColsAtCompileTime)
            , "Covariance and observation dimensions must be equal.");

public:
    typedef FilterBase<observation, covariance, robot_state> FB;
    /** \brief Gaussian Mixture Model */
    typedef GMM<observation, covariance> gmm;
    /** \brief Single gaussian */
    typedef typename gmm::gauss gauss;
    /** \brief Vector of observation - one single-robot measurement */
    typedef typename gauss::vec_state vec_observation;
    /** \brief Vector of covariances */
    typedef typename gauss::vec_covariance vec_covariance;
    /** \brief Vector of the robot states - one single-robot state.
     *  \details First state define measurements reference point.
     * The rest of the states are used to compute fov function.*/
    typedef std::vector<robot_state, Eigen::aligned_allocator<robot_state> > vec_robot_state;

    /**\brief Vector of vector of observation - one multiple-robot measurement */
    typedef typename std::vector<vec_observation> vec_vec_observation;
    /**\brief Vector of vector of robot state - one multiple-robot state */
    typedef typename std::vector<vec_robot_state> vec_vec_robot_state;

    /** FOV function type, take observation, robot state and id of the robot.
     * Can be used for default FOV function shadowing. */
    typedef typename boost::function<
    bool(const observation&, const robot_state&, unsigned int)> fov_function_ptr;
    /** Probability function type, take observation robot state and id of the robot.
     * Can be used for pd and ck function shadowing. */
    typedef typename boost::function<
    double(const observation&, const robot_state&, unsigned int)> prob_function_ptr;

public:
    /** \brief Default destructor */
    virtual ~FilterBase() {
    }

    /** \brief Pure virtual function for duplicating derived objects.
     *  \details call FilterBase::initialization(obj) to duplicate interface parameters */
    virtual FilterBase * clone(void) const = 0;

    /** \brief Pure virtual multiple-robot update function.
     *  \param vec_observations - vector of observation for each robot
     *  \param vec_rss - vector of robot_states for each robot - one robot state is vector as well */
    virtual void update(const vec_vec_observation& vec_observations,
            const vec_vec_robot_state& vec_rss) = 0;

    /** \brief Pure virtual multiple-robot prediction function
     *   \param vec_observations - vector of observation for each robot
     *   \param vec_rss - vector of robot_states for each robot - one robot state is vector as well */
    virtual void predict(const vec_vec_observation& observations,
            const vec_vec_robot_state& rss) = 0;

    /** \brief Single robot update function
     *  \details This function call multiple-robot update with number of robots set to 1 */
    void update(const vec_observation& observations,
            const vec_robot_state& rss) {
        vec_vec_observation vec_observations;
        vec_vec_robot_state vec_rss;
        vec_observations.push_back(observations);
        vec_rss.push_back(rss);
        update(vec_observations, vec_rss);
    }

    /** \brief Single robot predict function
     *  \details This function call multiple-robot predict with number of robots set to 1 */
    void predict(const vec_observation& observations,
            const vec_robot_state& rss) {
        vec_vec_observation vec_observations;
        vec_vec_robot_state vec_rss;
        vec_observations.push_back(observations);
        vec_rss.push_back(rss);
        predict(vec_observations, vec_rss);
    }

    /** \brief Single robot update function
     *  \details This function call multiple-robot update with number of robots set to 1 */
    void update(const vec_observation& observations, const robot_state& rs =
            robot_state::Identity()) {
        vec_robot_state rss;
        rss.push_back(rs);
        update(observations, rss);
    }

    /** \brief Single robot predict function
     *  \details This function call multiple-robot predict with number of robots set to 1 */
    void predict(const vec_observation& observations, const robot_state& rs =
            robot_state::Identity()) {
        vec_robot_state rss;
        rss.push_back(rs);
        predict(observations, rss);
    }

    /** \brief Estimate the features.
     *  \details Get features with weight higher then Tmin */
    virtual void estimate(vec_observation& feature, double Tmin) const {
        BOOST_FOREACH(const gauss& g, phd.get_gaussians()) {
            if (g.get_w() > Tmin) {
                feature.push_back(g.get_mu());
            }
        }
    }

    /** \brief Prune the PHD.
     *  \param Dmin - Cluster components with with mahalanobis distance squared lower or equal Dmin.
     *  \param Tmin - Remove components with weight lowest then Tmin.
     *  \param Jmax - Maximum number of gaussians. */
    virtual void prune(double Dmin = 0, double Tmin = 0, unsigned int Jmax = 10000) {
        phd.prune(Dmin, Tmin, Jmax);
    }

    /** \brief Get estimated number of features */
    virtual double get_number_of_features() const {
        return phd.sum();
    }

    /** \brief Intensity of the clutter RFS
     *  \details Can be shadowed by set_ck_function if not set default value is computed */
    virtual double c(const observation& z = observation::Zero(),
            const robot_state& rs = robot_state::Identity(),
            unsigned int robot_id = 0) const {
        if (ck_ptr) {
            return ck_ptr(z, rs, robot_id);
        }
        return CK;
    }

    /** \brief Probability of the feature detection.
     *  \details we can avoid FOV computation in default if we have prior information about visibility:
     *       1-visible; 0-unvisible; -1-dont-known
     *  \reture PD if is in FOV and 0.0 otherwise*/
    virtual double pd(const observation& feature, const vec_robot_state& rss,
            unsigned int robot_id = 0, int is_visible = -1) const {
        if(pd_ptr) {
            return pd_ptr(feature, rss[0], robot_id);
        }
        if(is_visible == 1) {
            return PD;
        } else if(is_visible == 0) {
            return 0.0;
        }
        if (is_in_fov(feature, rss, robot_id)) {
            return PD;
        } else {
            return 0.0;
        }
    }

    /** \brief Probability of the feature detection. */
    double pd(const observation& feature, const robot_state& rs =
            robot_state::Identity(), unsigned int robot_id = 0, int is_visible = -1) const {
        vec_robot_state rss;
        rss.push_back(rs);
        return pd(feature, rss, robot_id, is_visible);
    }

    /** \brief Compute expected feature location from robot observation.
     *  \details mu = h_inverse(z,X). */
    virtual observation h_inverse(const observation& z,
            const robot_state& rs) const {
        const int rs_dim = int(robot_state::MatrixType::RowsAtCompileTime)-1;
        const int obs_dim = int(observation::RowsAtCompileTime);
        if(rs_dim == obs_dim) {
            return rs * z;
        } else { //observation is higher than robot state so make matrix manually
            typename robot_state::VectorType t;
            t = z.head(rs_dim);
            t = rs * t;
            observation mu = z;
            mu.head(rs_dim) = t;
            return mu;
        }
    }

    /** \brief Predict observation based on feature location and robot state */
    virtual observation h(const observation& mu, const robot_state& rs) const {
        const int rs_dim = int(robot_state::MatrixType::RowsAtCompileTime)-1;
        const int obs_dim = int(observation::RowsAtCompileTime);
        if(rs_dim == obs_dim) {
            return rs.inverse() * mu;
        } else { //observation is higher than robot state so make matrix manually
            typename robot_state::VectorType t;
            t = mu.head(rs_dim);
            t = rs.inverse() * t;
            observation z = mu;
            z.head(rs_dim) = t;
            return z;
        }
    }

    /** \brief Jacobian of the measurement model. */
    virtual covariance H(const observation& mu, const robot_state& rs) const {
        const int rs_dim = int(robot_state::MatrixType::RowsAtCompileTime)-1;
        const int obs_dim = int(observation::RowsAtCompileTime);
        if(rs_dim == obs_dim) {
            return rs.rotation().inverse();
        } else {
            covariance jacobian = covariance::Identity();
            jacobian.block(0,0,robot_state::Dim,robot_state::Dim) = rs.rotation().inverse();
            return jacobian;
        }
    }

    /** \brief Get covariance of measurement model */
    virtual covariance R(unsigned int robot_id = 0) {
        if(robot_id < model_R.size())
        return model_R[robot_id];
        else
        return R_default;
    }

    /** \brief Decide whether feature in the FOV of the robot.
     *  \deatils Return true if feature can be observed from at least one robot state*/
    virtual bool is_in_fov(const observation& feature,
            const vec_robot_state& rss, unsigned int robot_id = 0) const {
        BOOST_FOREACH(const robot_state& rs, rss) {
            if (is_in_fov(feature, rs, robot_id)) {
                return true;
            }
        }
        return false;
    }

    /** \brief Decide whether feature in the FOV of the robot.
     *  \details The default FOV based on parameter is used if not shadowed.
     *  In default FOV there is an assumption that FOV is equal for each robot.*/
    virtual bool is_in_fov(const observation& feature, const robot_state& rs = robot_state::Identity(),
            unsigned int robot_id = 0) const {
        if (fov) {
            return fov(feature, rs, robot_id);
        }
        //test distance to feature
        observation z = h(feature, rs);
        double dist = z.norm();
        if(dist > fov_distance) {
            return false;
        }
        if(dist < fov_distance_min) {
            return false;
        }

        //check horizontal angle
        double angleH = atan2((double)z(1), (double)z(0));
        if(fabs(2.0 * angleH) > fov_H_angle) { // two times because angle is only from one side of x axis
            return false;
        }

        if(observation::RowsAtCompileTime > 2) {
            double angleV = atan2((double)z(2), (double)z(0));
            if(fabs(2.0 * angleV) > fov_V_angle) { // two times because angle is only from one side of x axis
                return false;
            }
        }

        return true;
    }

    /** \brief Set pointer to the FOV function. Default is used if not set*/
    void set_fov_function(fov_function_ptr f) {
        fov = f;
    }
    /** \brief Set Probability of detection function. Default is used if not set. */
    void set_pd_function(prob_function_ptr f) {
        pd_ptr = f;
    }
    /** \brief Set clutter function. Default is used if not set. */
    void set_ck_function(prob_function_ptr f) {
        ck_ptr = f;
    }

    /** \brief Initialize object based on instance filter*/
    void initialize(const FB& filter);

    /** \brief Initialize object with default values */
    void initialize();

public:
    gmm phd;
    double PD; //Probability of feature detection if it is in FOV
    double CK;//Clutter
    covariance R_default;
    vec_covariance model_R;//measurement noise covariance matrix

    double fov_H_angle;//default fov function horizontal angle
    double fov_V_angle;//default fov function vertical angle
    double fov_distance;//default fov distance
    double fov_distance_min;//default fov distance

private:
    fov_function_ptr fov;
    prob_function_ptr pd_ptr;
    prob_function_ptr ck_ptr;

};
#endif

template<class observation, class covariance, class robot_state>
inline void FilterBase<observation, covariance, robot_state>::initialize(
        const FB& filter) {
    PD = filter.PD;
    CK = filter.CK;
    R_default = covariance(filter.R_default);
    model_R = vec_covariance(filter.model_R);
    fov_H_angle = filter.fov_H_angle;
    fov_V_angle = filter.fov_V_angle;
    fov_distance = filter.fov_distance;
    fov_distance_min = filter.fov_distance_min;

    fov = filter.fov;
    pd_ptr = filter.pd_ptr;
    ck_ptr = filter.ck_ptr;
    phd = filter.phd;
}

template<class observation, class covariance, class robot_state>
inline void FilterBase<observation, covariance, robot_state>::initialize() {
    PD = 0.95;
    CK = pow(10, -4);
    R_default = covariance::Identity() * .02;
    fov_H_angle = M_PI;
    fov_V_angle = M_PI;
    fov_distance = 2.0;
    fov_distance_min = 0.0;
}
