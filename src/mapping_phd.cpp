/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 27, 2013
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Example of using FilterPHD class for 2d features
 */

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>

#include <geometry_msgs/PoseStamped.h>
#include <rfs_slam/FilterPHD.h>
#include <rfs_slam/EnvironmentSim.h>

using namespace Eigen;
typedef FilterPHD<Vector2d, Matrix2d, Affine2d> FPHD;

boost::shared_ptr<FPHD::FB> filter;
boost::shared_ptr<ros::Publisher> pub_phd;
boost::shared_ptr<EnvironmentSim> env;

void publish_phd();
void publish_transform(const Affine2d& rs, const std::string& frame);

int main(int argc, char **argv) {
    ros::init(argc, argv, "phd_mapping");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);

    pub_phd = boost::make_shared<ros::Publisher>(
            node.advertise<pcl::PointCloud<pcl::PointXYZI> >("/phd", 1));
    ros::Duration(1).sleep();
    FPHD f;
    f.JMax = 500;
    f.Dmin = 0.5;
    f.Tmin = .00001;
    f.CK = 0.5;
    f.PD = 1;
    f.R_default = Matrix2d::Identity() * 0.2;
    f.fov_H_angle = M_PI_2;
    f.fov_V_angle = M_PI_2;
    f.fov_distance = 4.0;
    filter = boost::make_shared<FPHD>(f);

    env.reset(new EnvironmentSim(100));
    env->fov_distance = f.fov_distance;
    env->fov_H_angle = f.fov_H_angle;
    env->fov_V_angle = f.fov_V_angle;
    env->fov_distance_min = f.fov_distance_min;
    env->noise_measurement = 0.01;

    FPHD::vec_robot_state trajectory, trajectory_2;
    env->generate_circle_trajectory(2.0, Vector2d(5, 0), 100, trajectory);
    env->generate_circle_trajectory(1.0, Vector2d(-5, 0), 100, trajectory_2);

    while (true) {
        for (unsigned int i = 0; i < trajectory.size(); ++i) {
            Affine2d rs = trajectory[i];
            Affine2d rs_2 = trajectory_2[i];
            publish_transform(rs, "base_link");
            publish_transform(rs_2, "base_link_2");

            FPHD::vec_vec_observation vec_obsvs;
            FPHD::vec_vec_robot_state vec_rss;
            {
                FPHD::vec_observation obsvs;
                env->get_observation(obsvs, rs);
                FPHD::vec_robot_state rss(1, rs);
                vec_obsvs.push_back(obsvs);
                vec_rss.push_back(rss);
            }
            {
                FPHD::vec_observation obsvs;
                env->get_observation(obsvs, rs_2);
                FPHD::vec_robot_state rss(1, rs_2);
                vec_obsvs.push_back(obsvs);
                vec_rss.push_back(rss);
            }
            ros::Time start = ros::Time::now();
            ROS_INFO_STREAM("After start size: " << filter->phd.size());
            filter->predict(vec_obsvs, vec_rss);
            filter->update(vec_obsvs, vec_rss);
            filter->prune();

            publish_phd();
            ROS_INFO_STREAM("Number of features: " << filter->get_number_of_features());
            //ros::Duration(2.0).sleep();
            if (!ros::ok())
                return 1;
        }
    }
    return 0;
}

void publish_transform(const Affine2d& rs, const std::string& frame) {
    static tf::TransformBroadcaster br;
    tf::StampedTransform stamped_transform;
    stamped_transform.frame_id_ = "world";
    stamped_transform.child_frame_id_ = frame;
    stamped_transform.stamp_ = ros::Time::now();
    Affine3d e;
    e.setIdentity();
    Vector3d v(0, 0, 0);
    v.head(2) = rs.translation();
    Matrix3d rot = Matrix3d::Identity();
    rot.topLeftCorner(2, 2) = rs.rotation();
    e.translate(v).rotate(rot);
    tf::transformEigenToTF(e, stamped_transform);
    br.sendTransform(stamped_transform);
}

void publish_phd() {
    if (!pub_phd)
        return;
    if (pub_phd->getNumSubscribers() == 0)
        return;
    float voxel_size = 0.05;
    pcl::PointCloud<pcl::PointXYZI>::Ptr pc(
            new pcl::PointCloud<pcl::PointXYZI>);

    BOOST_FOREACH(const FPHD::gauss& g, filter->phd.get_gaussians()) {
        std::vector<double> eigs;
        FPHD::vec_observation eigvs;
        g.eigen_value_vectors(eigs, eigvs);
        double length = *std::max_element(eigs.begin(), eigs.end());
        length = sqrt(length) * 2;
        if (length > 1.0) {
            ROS_WARN_STREAM("Eigen value 2*sqrt() too large: " << length);
            length = 1.0;
        }
        if (length < 0.05) {
            ROS_WARN_STREAM("Eigen value 2*sqrt() too small: " << length);
            length = 0.1;
        }
        for (float xx = -length; xx <= length; xx += voxel_size) {
            for (float yy = -length; yy <= length; yy += voxel_size) {
                Vector2d state = g.get_mu();
                state(0) += xx;
                state(1) += yy;
                pcl::PointXYZI p;
                p.x = state(0);
                p.y = state(1);
                p.z = 0.0;
                p.intensity = filter->phd.pdf(state);
                //p.z += (p.intensity);
                pc->push_back(p);
            }
        }
    }

    std_msgs::Header header;
    header.frame_id = "world";
    header.stamp = ros::Time::now();
    pc->header = pcl_conversions::toPCL(header);

    pcl::VoxelGrid<pcl::PointXYZI> sor;
    sor.setInputCloud(pc);
    sor.setLeafSize(voxel_size, voxel_size, voxel_size);

    pcl::PointCloud<pcl::PointXYZI> outpc;
    sor.filter(outpc);
    pub_phd->publish(outpc);

}
