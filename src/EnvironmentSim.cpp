/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Nov 28, 2013
 *     Author: Vladimir Petrik <vladko.petrik@gmail.com>
 */

#include "rfs_slam/EnvironmentSim.h"
#include <boost/random/uniform_real.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <ostream>
#include <fstream>

EnvironmentSim::EnvironmentSim() :
    map_dim_min(-10.0, -10.0, 0.0), map_dim_max(10.0, 10.0, 0.0), rng(1) {
    generate_random_features();
    noise_measurement = 0.0;
    lambda_c = 0.0;
}

EnvironmentSim::~EnvironmentSim() {
}

EnvironmentSim::EnvironmentSim(unsigned int num_of_features) :
    map_dim_min(-10.0, -10.0, 0.0), map_dim_max(10.0, 10.0, 0.0), rng(1) {
    generate_random_features(num_of_features);
    lambda_c = 0.0;
}

EnvironmentSim::EnvironmentSim(unsigned int num_of_features,
                               const Vector3d& map_dim_min, const Vector3d& map_dim_max) :
    map_dim_min(map_dim_min), map_dim_max(map_dim_max), rng(1) {
    generate_random_features(num_of_features);
    lambda_c = 0.0;
}

void EnvironmentSim::generate_random_features(unsigned int num_of_features) {
    boost::mt19937 rng(1);
    boost::uniform_real<> dist(0, 1);
    for(unsigned int i = 0; i < num_of_features; ++i) {
        Vector3d v(0, 0, 0);
        for(unsigned int j = 0; j < 3; ++j) {
            v(j) = dist(rng) * (map_dim_max(j) - map_dim_min(j)) + map_dim_min(j);
        }
        features.push_back(v);
    }
}

Vector2d EnvironmentSim::h2(const Vector3d& feature, const Affine2d& rs) const {
    Vector2d t = feature.head(2);
    return rs.inverse() * t;
}

Vector3d EnvironmentSim::h3(const Vector3d& feature, const Affine2d& rs) const {
    Vector3d z;
    Vector2d t = h2(feature, rs);
    z.head(2) = t;
    z(2) = feature(2);
    return z;
}

bool EnvironmentSim::is_in_fov(const Vector3d& feature,
                               const Affine2d& rs) const {

    Vector3d z = h3(feature, rs);
    double dist = z.norm();
    if(dist > fov_distance) {
        return false;
    }
    if(dist < fov_distance_min) {
        return false;
    }

    //check horizontal angle
    double angleH = atan2((double) z(1), (double) z(0));
    if(fabs(2.0 * angleH) > fov_H_angle) {  // two times because angle is only from one side of x axis
        return false;
    }

    double angleV = atan2((double) z(2), (double) z(0));
    if(fabs(2.0 * angleV) > fov_V_angle) {  // two times because angle is only from one side of x axis
        return false;
    }

    return true;
}

void EnvironmentSim::get_observation(
    vector<Vector3d, aligned_allocator<Vector3d> >& observation,
    const Affine2d& rs) {
    observation.clear();
    boost::normal_distribution<> dist(0.0, noise_measurement);

    BOOST_FOREACH(const Vector3d & feature, features) {
        if(is_in_fov(feature, rs)) {
            Vector3d v = h3(feature, rs) + Vector3d(dist(rng), dist(rng), dist(rng));
            observation.push_back(v);
        }
    }
}

void EnvironmentSim::get_observation(
    vector<Vector2d, aligned_allocator<Vector2d> >& observation,
    const Affine2d& rs) {
    observation.clear();
    boost::normal_distribution<> dist(0.0, noise_measurement);
    BOOST_FOREACH(const Vector3d & feature, features) {
        if(is_in_fov(feature, rs)) {
            Vector2d v = h2(feature, rs) + Vector2d(dist(rng), dist(rng));
            observation.push_back(v);
        }
    }

    double V_large = M_PI * pow(fov_distance, 2);
    double V_small = M_PI * pow(fov_distance_min, 2);

    double scale = 1.0 - fov_H_angle / (2.0 * M_PI) ;
    double V = scale * (V_large - V_small);

    double n_clutter = V * lambda_c;

    boost::uniform_real<> dist_angle(-0.5 * fov_H_angle, 0.5 * fov_H_angle);
    boost::uniform_real<> dist_d(fov_distance_min, fov_distance);
    for(int i = 0; i < n_clutter; i++) {
        double ang = dist_angle(rng);
        double d = dist_d(rng);
        Vector2d v(cos(ang) * d, sin(ang) * d);
        observation.push_back(v);
    }

}

void EnvironmentSim::generate_circle_trajectory(double radius,
        const Vector2d& center, unsigned int n,
        vector<Affine2d, aligned_allocator<Affine2d> >& traj, double angle_offset) {

    for(unsigned int i = 0; i < n; ++i) {
        double angle = 2.0 * M_PI / ((double) n - 1) * ((double) i);
        angle += angle_offset;
        Affine2d rs = Affine2d::Identity();
        rs.translate(center).rotate(Rotation2Dd(angle)).translate(
            Vector2d(radius, 0)).rotate(Rotation2Dd(M_PI_2));
        traj.push_back(rs);
    }

}

void EnvironmentSim::save_features(const std::string& filename) {
    std::ofstream of;
    of.open(filename.c_str(), std::ios::out);
    BOOST_FOREACH(const Eigen::Vector3d & f, features) {
        of << f(0) << ",";
        of << f(1) << ",";
        of << f(2);
        of << std::endl;
    }
    of.close();
}
