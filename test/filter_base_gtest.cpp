#include <gtest/gtest.h>
#include <rfs_slam/FilterPHD.h>
#include <boost/make_shared.hpp>
#include <math.h>
#include <boost/lambda/lambda.hpp>

typedef Eigen::Vector3d observation;
typedef Eigen::Matrix3d covariance;
typedef Eigen::Affine2d robot_state;
typedef FilterPHD<observation, covariance, robot_state> Filter;
typedef FilterBase<observation, covariance, robot_state> FB;
typedef FilterPHD<observation, covariance, robot_state> FPHD;

TEST (FilterBase, ConstructorObject) {
    Filter f, g;
    f.initialize(); //initialized and non initialized must be equal
    EXPECT_DOUBLE_EQ(f.PD, g.PD);
    EXPECT_DOUBLE_EQ(f.CK, g.CK);
    EXPECT_EQ(f.R_default, g.R_default);
    EXPECT_DOUBLE_EQ(f.fov_H_angle, g.fov_H_angle);
    EXPECT_DOUBLE_EQ(f.fov_V_angle, g.fov_V_angle);
    EXPECT_DOUBLE_EQ(f.fov_distance, g.fov_distance);
    EXPECT_DOUBLE_EQ(f.fov_distance_min, g.fov_distance_min);
    EXPECT_EQ(f.phd.size(), g.phd.size());
    f.PD = 0.0;
    f.CK = 0.0;
    f.R_default = covariance::Identity();
    f.fov_H_angle = 0.0;
    f.fov_V_angle = 0.0;
    f.fov_distance = 0.0;
    f.fov_distance_min = 0.0;
    f.phd.add_gaussian(observation::Zero(), covariance::Identity(), 1);
    g = Filter(f); //Test copy-ability
    EXPECT_DOUBLE_EQ(f.PD, g.PD);
    EXPECT_DOUBLE_EQ(f.CK, g.CK);
    EXPECT_EQ(f.R_default, g.R_default);
    EXPECT_DOUBLE_EQ(f.fov_H_angle, g.fov_H_angle);
    EXPECT_DOUBLE_EQ(f.fov_V_angle, g.fov_V_angle);
    EXPECT_DOUBLE_EQ(f.fov_distance, g.fov_distance);
    EXPECT_DOUBLE_EQ(f.fov_distance_min, g.fov_distance_min);
    EXPECT_EQ(f.phd.size(), g.phd.size());
}

TEST (FilterBase, Initialization) {
    Filter f;
    f.initialize();
    EXPECT_DOUBLE_EQ(f.PD, 0.95);
    EXPECT_DOUBLE_EQ(f.CK, pow(10, -4));
    EXPECT_EQ(f.R_default, covariance::Identity() * 0.02);
    EXPECT_DOUBLE_EQ(f.fov_H_angle, (double)M_PI);
    EXPECT_DOUBLE_EQ(f.fov_V_angle, (double)M_PI);
    EXPECT_DOUBLE_EQ(f.fov_distance, 2.0);
    EXPECT_DOUBLE_EQ(f.fov_distance_min, 0.0);
    EXPECT_EQ(f.phd.size(), 0);

    f.PD = 0.0;
    f.CK = 0.0;
    f.R_default = covariance::Identity();
    f.fov_H_angle = 0.0;
    f.fov_V_angle = 0.0;
    f.fov_distance = 0.0;
    f.fov_distance_min = 0.0;
    f.phd.add_gaussian(observation::Zero(), covariance::Identity(), 1);
    Filter g;
    g.initialize(f); //Test copy ability
    EXPECT_DOUBLE_EQ(f.PD, g.PD);
    EXPECT_DOUBLE_EQ(f.CK, g.CK);
    EXPECT_EQ(f.R_default, g.R_default);
    EXPECT_DOUBLE_EQ(f.fov_H_angle, g.fov_H_angle);
    EXPECT_DOUBLE_EQ(f.fov_V_angle, g.fov_V_angle);
    EXPECT_DOUBLE_EQ(f.fov_distance, g.fov_distance);
    EXPECT_DOUBLE_EQ(f.fov_distance_min, g.fov_distance_min);
    EXPECT_EQ(f.phd.size(), g.phd.size());
}

TEST (FilterBase, Estimation) {
    FPHD f;
    f.phd.add_gaussian(observation::Zero(), covariance::Identity(), 1);
    f.phd.add_gaussian(observation::Zero(), covariance::Identity(), 0.1);
    f.phd.add_gaussian(observation::Zero(), covariance::Identity(), 0.98);
    f.phd.add_gaussian(observation::Zero(), covariance::Identity(), 0.6);
    f.phd.add_gaussian(observation::Zero(), covariance::Identity(), 0.2);
    FB::vec_observation est;
    f.estimate(est, 0.5);
    EXPECT_EQ(est.size(), 3);
}

TEST (FilterBase, ShadowingC) {
    using namespace boost::lambda;
    FPHD f;
    f.set_ck_function(constant(0.5));
    EXPECT_EQ(f.c(), 0.5);
}

TEST (FilterBase, ShadowingFOV) {
    using namespace boost::lambda;
    FPHD f;
    f.set_fov_function(true || _3);
    observation feature;
    robot_state rs;
    EXPECT_TRUE(f.is_in_fov(feature, rs));
}

TEST (FilterBase, ShadowingPD) {
    using namespace boost::lambda;
    FPHD f;
    f.set_pd_function(constant(0.5));
    observation feature;
    EXPECT_EQ(f.pd(feature), 0.5);
}

TEST (FilterBase, DefaultC) {
    FPHD f;
    EXPECT_DOUBLE_EQ(f.c(), f.CK);
}

TEST (FilterBase, DefaultR) {
    FPHD f;
    EXPECT_EQ(f.R(), f.R_default);
}

TEST (FilterBase, DefaultMultiR) {
    FPHD f;
    f.model_R.push_back(covariance::Identity() * 0.2);
    f.model_R.push_back(covariance::Identity() * -0.1);
    f.model_R.push_back(covariance::Identity() * -5.0);
    EXPECT_EQ(f.R(0), covariance::Identity() * 0.2);
    EXPECT_EQ(f.R(1), covariance::Identity() * -0.1);
    EXPECT_EQ(f.R(2), covariance::Identity() * -5.0);
}

TEST (FilterBase, DefaultPD) {
    FPHD f;
    observation feature;
    using namespace boost::lambda;
    f.set_fov_function(true || _3);
    EXPECT_DOUBLE_EQ(f.pd(feature), f.PD);
    f.set_fov_function(false && _3);
    EXPECT_DOUBLE_EQ(f.pd(feature), 0);
}

TEST (FilterBase, DefaultFOVOneState2D) {
    using namespace Eigen;
    FilterPHD<Vector2d, Matrix2d, Affine2d> f;
    f.fov_H_angle = M_PI;
    f.fov_V_angle = M_PI;
    f.fov_distance = 2.0;
    f.fov_distance_min = 0.05;
    std::vector<Vector2d, aligned_allocator<Vector2d> > features;
    std::vector<bool> visible;

    features.push_back(Vector2d(1.0, 0.0));
    visible.push_back(true);
    features.push_back(Vector2d(1.0, 1.0));
    visible.push_back(true);
    features.push_back(Vector2d(-1.0, -1.0));
    visible.push_back(false);
    features.push_back(Vector2d(1.0, 1.5));
    visible.push_back(true);
    features.push_back(Vector2d(1.0, 2.5));
    visible.push_back(false);
    features.push_back(Vector2d(0.04, 0.00));
    visible.push_back(false);

    Affine2d rs = Affine2d::Identity();
    for (unsigned int i = 0; i < features.size(); ++i) {
        EXPECT_EQ(f.is_in_fov(features[i], rs), visible[i]);
    }
}

TEST (FilterBase, DefaultFOVOneState3D2D) {
    using namespace Eigen;
    FilterPHD<Vector3d, Matrix3d, Affine2d> f;
    f.fov_H_angle = M_PI;
    f.fov_V_angle = M_PI;
    f.fov_distance = 2.0;
    f.fov_distance_min = 0.05;
    std::vector<Vector3d, aligned_allocator<Vector3d> > features;
    std::vector<bool> visible;

    features.push_back(Vector3d(1.0, 0.0, 0.0));
    visible.push_back(true);
    features.push_back(Vector3d(1.0, 1.0, 0.0));
    visible.push_back(true);
    features.push_back(Vector3d(-1.0, -1.0, 0.0));
    visible.push_back(false);
    features.push_back(Vector3d(1.0, 1.5, 0.0));
    visible.push_back(true);
    features.push_back(Vector3d(1.0, 2.5, 0.0));
    visible.push_back(false);
    features.push_back(Vector3d(0.04, 0.00, 0.0));
    visible.push_back(false);
    features.push_back(Vector3d(-0.0001, 0, -1.0));
    visible.push_back(false);
    features.push_back(Vector3d(0.1, 0, -1.0));
    visible.push_back(true);

    Affine2d rs = Affine2d::Identity();
    for (unsigned int i = 0; i < features.size(); ++i) {
        EXPECT_EQ(f.is_in_fov(features[i], rs), visible[i]);
    }
}

TEST (FilterBase, DefaultFOVMoreStates) {
    using namespace Eigen;
    FilterPHD<Vector3d, Matrix3d, Affine2d> f;
    f.fov_H_angle = M_PI;
    f.fov_V_angle = M_PI;
    f.fov_distance = 2.0;
    f.fov_distance_min = 0.05;
    std::vector<Vector3d, aligned_allocator<Vector3d> > features;
    std::vector<bool> visible;

    features.push_back(Vector3d(1.0, 0.0, 0.0)); //visible only from second pose
    visible.push_back(true);
    features.push_back(Vector3d(-1.0, 0.0, 0.0)); //visible only from first pose
    visible.push_back(true);
    features.push_back(Vector3d(5.0, 0.0, 0.0)); //not visible at all
    visible.push_back(false);

    std::vector<Affine2d, aligned_allocator<Affine2d> > rss;
    Affine2d rs = Affine2d::Identity();
    rs.translate(Vector2d(-2.0, 0));
    rss.push_back(rs);
    rs.setIdentity();
    rss.push_back(rs);
    for (unsigned int i = 0; i < features.size(); ++i) {
        EXPECT_EQ(f.is_in_fov(features[i], rss), visible[i]);
    }
}

TEST (FilterBase, Model_3D_3D) {
    //This is state depend test so don't use defined types
    using namespace Eigen;
    FilterPHD<Vector3d, Matrix3d, Affine3d> f;
    Vector3d feature(5.0, 1.0, 2.0);
    Vector3d obsv(-2.0, -3.0, 2.0);
    Affine3d rs;
    rs.setIdentity();
    rs.translate(Vector3d(2.0, 3.0, 0.0)).rotate(
            AngleAxisd(M_PI_2, Vector3d::UnitZ()));

    Matrix3d jacobian; //inverse of rotation
    jacobian << 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 1.0;

    EXPECT_TRUE(feature.isApprox(f.h_inverse(obsv, rs)));
    EXPECT_TRUE(obsv.isApprox(f.h(feature, rs)));
    EXPECT_TRUE(jacobian.isApprox(f.H(feature, rs)));
}

TEST (FilterBase, Model_2D_2D) {
    //This is state depend test so don't use defined types
    using namespace Eigen;
    FilterPHD<Vector2d, Matrix2d, Affine2d> f;
    Vector2d feature(5.0, 1.0);
    Vector2d obsv(-2.0, -3.0);
    Affine2d rs;
    rs.setIdentity();
    rs.translate(Vector2d(2.0, 3.0)).rotate(Rotation2Dd(M_PI_2));

    Matrix2d jacobian; //inverse of rotation
    jacobian << 0.0, 1.0, -1.0, 0.0;

    EXPECT_TRUE(feature.isApprox(f.h_inverse(obsv, rs)));
    EXPECT_TRUE(obsv.isApprox(f.h(feature, rs)));
    EXPECT_TRUE(jacobian.isApprox(f.H(feature, rs)));
}

TEST (FilterBase, Model_3D_2D) {
    //This is state depend test so don't use defined types
    using namespace Eigen;
    FilterPHD<Vector3d, Matrix3d, Affine2d> f;
    Vector3d feature(5.0, 1.0, 2.0);
    Vector3d obsv(-2.0, -3.0, 2.0);
    Affine2d rs;
    rs.setIdentity();
    rs.translate(Vector2d(2.0, 3.0)).rotate(Rotation2Dd(M_PI_2));

    Matrix3d jacobian; //inverse of rotation
    jacobian << 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 1.0;

    EXPECT_TRUE(feature.isApprox(f.h_inverse(obsv, rs)));
    EXPECT_TRUE(obsv.isApprox(f.h(feature, rs)));
    EXPECT_TRUE(jacobian.isApprox(f.H(feature, rs)));
}

int main(int argc, char **argv) {
    try {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    } catch (std::exception &e) {
        std::cerr << "Unhandled Exception: " << e.what() << std::endl;
    }
    return 1;
}
