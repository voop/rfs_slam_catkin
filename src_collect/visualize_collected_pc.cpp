/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 04/24/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Visualize collected point clounds
 */

#include <ros/ros.h>
#include "CollectFnct.h"
#include <tf_conversions/tf_eigen.h>
#include <pcl/visualization/common/float_image_utils.h>
using namespace Eigen;

/** \brief Load timestamped positions from the file */
bool load_positions(const std::string& file, vector<ros::Time>& times, affine_vec& poses);
/** \brief Get the closest position */
bool get_pose(const ros::Time& timestamp, const vector<ros::Time>& times, const affine_vec& poses, Affine2d& pose);

bool update;
boost::mutex updateModelMutex;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr pc_visRGB(new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::PointCloud<pcl::PointXYZ>::Ptr pc_vis(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr pc_vis2(new pcl::PointCloud<pcl::PointXYZ>);

void pass_throught_xtion(pcl::PCLPointCloud2::Ptr& pc) {
    pcl::PCLPointCloud2::Ptr fptcloud(new pcl::PCLPointCloud2());
    pcl::PassThrough<pcl::PCLPointCloud2> pass;
    pass.setInputCloud(pc);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0.8, 5.5);
    pass.filter(*fptcloud);
    pc = fptcloud;
}

void transform_pc(pcl::PCLPointCloud2& pc, const tf::Transform& t) {
    sensor_msgs::PointCloud2 rpc, rpcout;
    pcl_conversions::fromPCL(pc, rpc);
    pcl_ros::transformPointCloud("odom", t, rpc, rpcout);
    pcl_conversions::toPCL(rpcout, pc);
}

void visualize() {
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(150, 150, 150);
    //viewer->addCoordinateSystem(1.0, Eigen::Affine3f::Identity());
    viewer->initCameraParameters();

    while(!viewer->wasStopped() && ros::ok()) {
        viewer->spinOnce(50);
        boost::mutex::scoped_lock updateLock(updateModelMutex);
        if(update) {
            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> c1(pc_vis, 125, 125, 125);
            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> c2(pc_vis2, 0, 255, 0);
//             if(!viewer->updatePointCloud(pc_vis, c1, "sample cloud"))
//                 viewer->addPointCloud(pc_vis, c1, "sample cloud");

            /*if(!viewer->updatePointCloud(pc_vis2, c2, "sift")) {
                viewer->addPointCloud(pc_vis2, c2, "sift");
                viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "sift");
            }*/
            if(!viewer->updatePointCloud(pc_visRGB, "RGBcloud"))
                viewer->addPointCloud(pc_visRGB, "RGBcloud");
	    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "RGBcloud");
            update = false;
        }
        updateLock.unlock();
    }
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "visualize_collected_pc");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    boost::thread workerThread(visualize);

    CollectFnct cf;
    std::string folder;
    node.param("/folder", folder, std::string("") + getenv("HOME") + "/.ros/rfs_collect/");
    fs::path path(folder + "pc");
    std::string input;
    node.param<std::string>("in", input, "slam_inputs.csv");

    std::vector<fs::path> files;
    cf.get_all_files(path, files);

    std::string ts, te;
    node.param<std::string>("/time_start", ts, "2014-04-25 00:04:00.000");
    node.param<std::string>("/time_end", te, "2016-04-25 00:04:00.000");
    ros::Time start_time = ros::Time::fromBoost(boost::posix_time::time_from_string(ts));
    ros::Time end_time = ros::Time::fromBoost(boost::posix_time::time_from_string(te));

    pcl::PCLPointCloud2::Ptr fcloud(new pcl::PCLPointCloud2());
    BOOST_FOREACH(fs::path & f, files) {
        ros::Time t = cf.extract_time(f.filename().c_str());
        if(t < start_time || t > end_time)
            continue;
        cf.print_time(t);
        ROS_INFO("%20f", t.toSec());

        affine_vec poses;
        vector<ros::Time> poses_times;
        load_positions(folder + input, poses_times, poses);

        Eigen::Affine2d rs = Eigen::Affine2d::Identity();
        if(!get_pose(t, poses_times, poses, rs))
            continue;

        pcl::PCLPointCloud2Ptr pc(new pcl::PCLPointCloud2);
        pcl::io::loadPCDFile(f.c_str(), *pc);
        pass_throught_xtion(pc);

        tf::Transform odom;
        tf::Transform b2x;

        {
            //Transform
            Rotation2Dd rot(0.0);
            rot.fromRotationMatrix(rs.linear());
            double angle = rot.angle();
            odom.setIdentity();
            odom.setOrigin(tf::Vector3(rs.translation()[0], rs.translation()[1], 0.0));
            odom.setRotation(tf::createQuaternionFromYaw(angle));

            b2x.setIdentity();
            b2x.setOrigin(tf::Vector3(-0.25, 0.175, 0.630));
            b2x.setRotation(tf::createQuaternionFromRPY(-M_PI_2 - M_PI / 32.0, 0.0, -M_PI_2));
        }

        transform_pc(*pc, odom * b2x);
        pcl::concatenatePointCloud(*pc, *fcloud, *pc);
        pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
        sor.setLeafSize(0.05f, 0.05f, 0.05f);
        sor.setInputCloud(pc);
        fcloud.reset(new pcl::PCLPointCloud2());
        sor.filter(*fcloud);

        boost::mutex::scoped_lock updateLock(updateModelMutex);
        pcl::fromPCLPointCloud2<pcl::PointXYZ>(*fcloud, *pc_vis);
        pc_visRGB->clear();
        BOOST_FOREACH(pcl::PointXYZ & p, *pc_vis) {
            pcl::PointXYZRGB pt;
            pt.x = p.x;
            pt.y = p.y;
            pt.z = p.z;
            //pcl::visualization::FloatImageUtils ut;
            //ut.getColorForFloat(p.z, pt.r, pt.g, pt.b);
	    pt.g = pt.r = 0;
	    pt.b = 255 * (1 - p.z / 2.0);

            pc_visRGB->push_back(pt);
        }
        update = true;
        updateLock.unlock();
        {
            fs::path psift(folder + "sift/" + f.filename().c_str());
            pcl::PCLPointCloud2 pc;
            pcl::io::loadPCDFile(psift.c_str(), pc);
            transform_pc(pc, odom * b2x);

            boost::mutex::scoped_lock updateLock(updateModelMutex);
            pcl::fromPCLPointCloud2<pcl::PointXYZ>(pc, *pc_vis2);
            update = true;
            updateLock.unlock();
        }

        ROS_INFO_STREAM("Press enter for next cloud");
        //getchar();
        if(!ros::ok())
            break;
    }

    workerThread.interrupt();
    workerThread.join();

    return 0;
}



bool get_pose(const ros::Time& timestamp, const vector<ros::Time>& times, const affine_vec& poses, Affine2d& pose) {
    int int_t_low = -1;

    for(unsigned int i = 0; i < times.size() - 1; ++i) {
        if(times[i] < timestamp && times[i + 1] > timestamp) {
            int_t_low = i;
            break;
        }
    }
    if(int_t_low == -1) {
        ROS_INFO_STREAM(timestamp);
        ROS_ERROR_STREAM("Cannot find satisfied timestamp");
        return false;
    }

    ros::Duration dt_low = times[int_t_low] - timestamp;
    ros::Duration dt_up = times[int_t_low + 1] - timestamp;
    if(fabs(dt_low.toSec()) > 0.5 && fabs(dt_up.toSec() > 0.5)) {
        ROS_ERROR_STREAM("Too large time offsets: " << dt_up.toSec());
        return false;
    }
    if(fabs(dt_low.toSec()) < fabs(dt_up.toSec())) {
        pose = poses[int_t_low];
    } else {
        pose = poses[int_t_low + 1];
    }
    return true;
}


bool load_positions(const std::string& file, vector<ros::Time>& times, affine_vec& poses) {
    std::ifstream f(file.c_str());
    if(!f.is_open()) {
        ROS_ERROR_STREAM("Cannot open file: " << file);
        return false;
    }
    char c;
    double x, y, theta, sec;
    while(!f.eof()) {
        f >> sec >> c >> x >> c >> y >> c >> theta;
        Affine2d e = Affine2d::Identity();
        e.translation() = Vector2d(x, y);
        e.linear() = Rotation2Dd(theta).toRotationMatrix();

        ros::Time t;
        t.fromSec(sec);

        poses.push_back(e);
        times.push_back(t);
    }
    return true;
}
