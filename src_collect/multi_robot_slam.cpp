#include <ros/ros.h>
#include <tf_conversions/tf_eigen.h>
#include <rfs_slam/RosUtils.h>
#include "Particle.h"
#include "CollectFnct.h"

typedef std::pair<FPHD::vec_robot_state, FPHD::vec_vec_observation> RobotsInput;

bool load_robot_inputs(const std::string& folder, std::vector<RobotsInput>& rinputs,
                       vector<ros::Time>& times, ros::Time start_time, ros::Time end_time);

/** \brief Load timestamped positions from the file */
bool load_positions(const std::string& file, vector<ros::Time>& times, affine_vec& poses);
/** \brief Get the closest position */
bool get_pose(const ros::Time& timestamp, const vector<ros::Time>& times, const affine_vec& poses, Affine2d& pose);

void save_avarage_pose(FPHD::vec_robot_state& poses, const std::string& filename, bool first, ros::Time time);
void save_maximum_pose(const Affine2d& rs, const std::string& filename, bool first, ros::Time time);
void save_statistic(const std::string& filename, bool first, ros::Time time, double num_features, double num_sifts) ;

void transform_pc(pcl::PCLPointCloud2& pc, const tf::Transform& t) {
    sensor_msgs::PointCloud2 rpc, rpcout;
    pcl_conversions::fromPCL(pc, rpc);
    pcl_ros::transformPointCloud("odom", t, rpc, rpcout);
    pcl_conversions::toPCL(rpcout, pc);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "single_robot_slam");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    std::string folder;
    node.param<std::string>("/folder", folder, "/home/voop/.ros/rfs_collect/");
    folder = "/tmp/rfs_multi/";

    std::string ts, te;
    node.param<std::string>("/time_start", ts, "2014-04-25 00:04:00.000");
    node.param<std::string>("/time_end", te, "2016-04-25 00:04:00.000");
    ros::Time start_time = ros::Time::fromBoost(boost::posix_time::time_from_string(ts));
    ros::Time end_time = ros::Time::fromBoost(boost::posix_time::time_from_string(te));

    vector<ros::Time> times;
    vector<ros::Time> times2, tmp;
    std::vector<RobotsInput> robots_intputs, robots_intputs1, robots_intputs2;
    if(!load_robot_inputs(std::string("") + getenv("HOME") + "/.ros/rfs_collect_robot1/", robots_intputs1, times, start_time, end_time))
        return -1;
    if(!load_robot_inputs(std::string("") + getenv("HOME") + "/.ros/rfs_collect_robot2/", robots_intputs2, times2, start_time, end_time))
        return -1;

    bool reverse_robots = false;
    if(robots_intputs1.size() > robots_intputs2.size()) {
        robots_intputs = robots_intputs1;
    } else {
        robots_intputs = robots_intputs2;
        bool reverse_robots = true;
        ROS_INFO_STREAM("Inputs2 is larger");
    }
    if(reverse_robots) {
        tmp = times;
        times = times2;
        times2 = tmp;
        for(int i = 0; i < robots_intputs1.size(); i++) {
            robots_intputs[i].first.push_back(robots_intputs1[i].first[0]);
            robots_intputs[i].second.push_back(robots_intputs1[i].second[0]);
        }
    } else {
        for(int i = 0; i < robots_intputs2.size(); i++) {
            robots_intputs[i].first.push_back(robots_intputs2[i].first[0]);
            robots_intputs[i].second.push_back(robots_intputs2[i].second[0]);
        }
    }

    Particle p;
    p.f.fov_distance = 5.5;
    p.f.fov_distance_min = 0.8;
    p.f.fov_H_angle = 58.0 * M_PI / 180.0;
    p.f.fov_V_angle = 45.0 * M_PI / 180.0;
    p.mm.a[0] = 0.15; // rad/rad
    p.mm.a[1] = 5 * (M_PI / 180.0); // rad/meter
    p.mm.a[2] = 0.05;// meters/meter
    p.mm.a[3] = 0.05; // meter/rad
    p.f.R_default = Matrix3d::Identity() * 0.25;
    p.f.CK = 0.05;
    p.f.PD = 0.95;


      
   
    Affine2d T = Affine2d::Identity();
    T.translation() = Vector2d(  8.8192,   -6.2295);
    T.linear() = Rotation2Dd(   -0.3298).toRotationMatrix();
    p.rss.push_back(Eigen::Affine2d::Identity());
    if(reverse_robots)
        p.rss.push_back(T.inverse());
    else
        p.rss.push_back(T);
    ParticleFilter<Particle> pf(200, p);

    bool first = true;
    for(unsigned int t = 0; t < robots_intputs.size(); ++t) {
        ROS_INFO_STREAM("Input: " << t << "/" << robots_intputs.size());
        RobotsInput ri = robots_intputs[t];
        BOOST_FOREACH(Particle & p, pf.particles) {
            p.motion(ri.first);
        }
        pf.set_weight_function(boost::bind(&Particle::compute_weight_phd_sum, _1, ri.second));
        pf.update_and_resample(false);

        ros::Time st = ros::Time::now();
        #pragma omp parallel for
        for(int p_i = 0; p_i < pf.particles.size(); ++p_i) {
            Particle& p = pf.particles[p_i];
            p.predict_update_prune(ri.second);
        }
        ROS_INFO_STREAM("Update prune time: " << ros::Time::now().toSec() - st.toSec());

        const Particle p = pf.get_maximum_weight_particle();
        ROS_INFO_STREAM("Number of observations: " << ri.second[0].size());
        FPHD::vec_robot_state poses;
        BOOST_FOREACH(const Particle & p, pf.particles) {
            BOOST_FOREACH(const Affine2d & rs, p.rss) {
                poses.push_back(rs);
            }
        }
        ROSUtils::publish_poses(poses, pf.weights);
        ROSUtils::save_poses(poses, pf.weights, "/tmp/poses.csv");
        
        if(times.size() > t) {
            poses.clear();
            BOOST_FOREACH(const Particle & p, pf.particles) {
                poses.push_back(p.rss[0]);
            }
            save_avarage_pose(poses, folder + "r1_average_poses.csv", first, times[t]);
            save_maximum_pose(p.rss[0], folder + "r1_maximum_poses.csv", first, times[t]);
            save_statistic(folder + "r1_statistics.csv", first, times[t], p.f.get_number_of_features(), ri.second[0].size());
        }

        if(times2.size() > t) {
            poses.clear();
            BOOST_FOREACH(const Particle & p, pf.particles) {
                poses.push_back(p.rss[1]);
            }
            save_avarage_pose(poses, folder + "r2_average_poses.csv", first, times2[t]);
            save_maximum_pose(p.rss[1], folder + "r2_maximum_poses.csv", first, times2[t]);
            save_statistic(folder + "r2_statistics.csv", first, times2[t], p.f.get_number_of_features(), ri.second[1].size());
        }


        if(!ros::ok())
            break;

        first = false;
    }

    spinner.stop();
    return 0;
}

void save_statistic(const std::string& filename, bool first, ros::Time time, double num_features, double num_sifts) {
    std::ofstream of;
    if(first) {
        of.open(filename.c_str(), std::ios::out);
    } else
        of.open(filename.c_str(), std::ios::out | std::ios::app);

    of.precision(20);
    of << time.toSec() << "," << num_features << "," << num_sifts;
    of << std::endl;
    of.close();
}

void save_maximum_pose(const Affine2d& rs, const std::string& filename, bool first, ros::Time time) {
    std::ofstream of;
    if(first) {
        of.open(filename.c_str(), std::ios::out);
    } else
        of.open(filename.c_str(), std::ios::out | std::ios::app);

    Vector2d avg_v = rs.translation();
    Rotation2Dd rot(0.0);
    rot.fromRotationMatrix(rs.linear());

    of.precision(20);
    of << time.toSec() << "," << avg_v[0] << "," << avg_v[1] << "," << rot.angle();
    of << std::endl;
    of.close();
}

void save_avarage_pose(FPHD::vec_robot_state& poses, const std::string& filename, bool first, ros::Time time) {
    std::ofstream of;
    if(first) {
        of.open(filename.c_str(), std::ios::out);
    } else
        of.open(filename.c_str(), std::ios::out | std::ios::app);

    Vector2d avg_v = Vector2d(0.0, 0.0);
    double sin_ang = 0, cos_ang = 0;

    BOOST_FOREACH(const Affine2d & rs, poses) {
        avg_v += rs.translation();
        Rotation2Dd rot(0.0);
        rot.fromRotationMatrix(rs.linear());
        sin_ang += sin(rot.angle());
        cos_ang += cos(rot.angle());
    }

    double n = poses.size();
    of.precision(20);
    of << time.toSec() << "," << avg_v[0] / n << "," << avg_v[1] / n << "," << atan2(sin_ang / n, cos_ang / n);
    of << std::endl;
    of.close();
}


bool load_robot_inputs(const std::string& folder, std::vector<RobotsInput>& rinputs, vector<ros::Time>& times,
                       ros::Time start_time, ros::Time end_time) {
    CollectFnct cf;
    fs::path path(folder + "sift");
    std::vector<fs::path> files;
    cf.get_all_files(path, files);

    affine_vec poses;
    vector<ros::Time> poses_times;
    load_positions(folder + "slam_inputs.csv", poses_times, poses);

    BOOST_FOREACH(fs::path & f, files) {
        RobotsInput ri;
        ros::Time t = cf.extract_time(f.filename().c_str());
        if(t < start_time || t > end_time)
            continue;
        Eigen::Affine2d rs = Eigen::Affine2d::Identity();
        if(!get_pose(t, poses_times, poses, rs))
            continue;
        ri.first.push_back(rs);

        pcl::PCLPointCloud2Ptr pc(new pcl::PCLPointCloud2);
        pcl::io::loadPCDFile(f.c_str(), *pc);

        tf::Transform b2x;
        b2x.setIdentity();
        b2x.setOrigin(tf::Vector3(-0.25, 0.175, 0.630));
        b2x.setRotation(tf::createQuaternionFromRPY(-M_PI_2, 0.0, -M_PI_2));
        transform_pc(*pc, b2x);

        pcl::PointCloud<pcl::PointXYZ> pcc;
        pcl::fromPCLPointCloud2<pcl::PointXYZ>(*pc, pcc);

        FPHD::vec_observation obsv;
        BOOST_FOREACH(const pcl::PointXYZ & point, pcc.points) {
            FPHD::vec_observation::value_type z(point.x, point.y, point.z);
            obsv.push_back(z);
        }
        ri.second.push_back(obsv);
        rinputs.push_back(ri);
        times.push_back(t);

        if(!ros::ok())
            return false;
    }


    if(rinputs.empty())
        return false;

    Affine2d last_rs = rinputs[0].first[0];
    for(int i = 0; i < rinputs.size(); i++) {
        Affine2d tmp = last_rs.inverse() * rinputs[i].first[0];
        last_rs = rinputs[i].first[0];
        rinputs[i].first[0] = tmp;
    }
    return true;
}



bool get_pose(const ros::Time& timestamp, const vector<ros::Time>& times, const affine_vec& poses, Affine2d& pose) {
    int int_t_low = -1;

    for(unsigned int i = 0; i < times.size() - 1; ++i) {
        if(times[i] < timestamp && times[i + 1] > timestamp) {
            int_t_low = i;
            break;
        }
    }
    if(int_t_low == -1) {
        ROS_INFO_STREAM(timestamp);
        ROS_ERROR_STREAM("Cannot find satisfied timestamp");
        return false;
    }

    ros::Duration dt_low = times[int_t_low] - timestamp;
    ros::Duration dt_up = times[int_t_low + 1] - timestamp;
    if(fabs(dt_low.toSec()) > 0.5 && fabs(dt_up.toSec() > 0.5)) {
        ROS_ERROR_STREAM("Too large time offsets: " << dt_up.toSec());
        return false;
    }
    if(fabs(dt_low.toSec()) < fabs(dt_up.toSec())) {
        pose = poses[int_t_low];
    } else {
        pose = poses[int_t_low + 1];
    }
    return true;
}


bool load_positions(const std::string& file, vector<ros::Time>& times, affine_vec& poses) {
    std::ifstream f(file.c_str());
    if(!f.is_open()) {
        ROS_ERROR_STREAM("Cannot open file: " << file);
        return false;
    }
    char c;
    double x, y, theta, sec;
    while(!f.eof()) {
        f >> sec >> c >> x >> c >> y >> c >> theta;
        Affine2d e = Affine2d::Identity();
        e.translation() = Vector2d(x, y);
        e.linear() = Rotation2Dd(theta).toRotationMatrix();

        ros::Time t;
        t.fromSec(sec);

        poses.push_back(e);
        times.push_back(t);
    }
    return true;
}






