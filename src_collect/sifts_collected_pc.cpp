/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 04/24/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Visualize collected point clounds
 */

#include <ros/ros.h>
#include "CollectFnct.h"
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/features/normal_3d.h>

namespace pcl {
    template<>
    struct SIFTKeypointFieldSelector<PointXYZ> {
        inline float
        operator()(const PointXYZ& p) const {
            return p.y;
        }
    };
}

void pass_throught_xtion(pcl::PCLPointCloud2::Ptr& pc) {
    pcl::PCLPointCloud2::Ptr fptcloud(new pcl::PCLPointCloud2());
    pcl::PassThrough<pcl::PCLPointCloud2> pass;
    pass.setInputCloud(pc);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0.8, 5.5);
    pass.filter(*fptcloud);
    pc = fptcloud;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "sift_collected_pc");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    CollectFnct cf;
    std::string folder;
    node.param("/folder", folder, std::string("") + getenv("HOME") + "/.ros/rfs_collect/");
    fs::path path(folder + "pc");

    std::vector<fs::path> files;
    cf.get_all_files(path, files);

    std::string ts, te;
    node.param<std::string>("/time_start", ts, "2014-04-25 00:04:00.000");
    node.param<std::string>("/time_end", te, "2016-04-25 00:04:00.000");
    ros::Time start_time = ros::Time::fromBoost(boost::posix_time::time_from_string(ts));
    ros::Time end_time = ros::Time::fromBoost(boost::posix_time::time_from_string(te));

    
    boost::filesystem::create_directories(folder + "sift");
    
    #pragma omp parallel for
    for(int i = 0; i < files.size(); i++) {
        fs::path f = files[i];
        ros::Time t = cf.extract_time(f.filename().c_str());
        if(t < start_time || t > end_time)
            continue;
        cf.print_time(t);

        pcl::PCLPointCloud2Ptr pc(new pcl::PCLPointCloud2);
        pcl::io::loadPCDFile(f.c_str(), *pc);
        
        const float min_scale = 0.05; 
        const int n_octaves = 6; 
        const int n_scales_per_octave = 4; 
        const float min_contrast = 0.003;

        pass_throught_xtion(pc);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::fromPCLPointCloud2<pcl::PointXYZ>(*pc, *cloud_xyz);

        // Estimate the sift interest points using z values from xyz as the Intensity variants
        pcl::SIFTKeypoint<pcl::PointXYZ, pcl::PointWithScale> sift;
        pcl::PointCloud<pcl::PointWithScale> result;
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ> ());
        sift.setSearchMethod(tree);
        sift.setScales(min_scale, n_octaves, n_scales_per_octave);
        sift.setMinimumContrast(min_contrast);
        sift.setInputCloud(cloud_xyz);
        sift.compute(result);

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_temp(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::copyPointCloud(result, *cloud_temp);
        ROS_INFO_STREAM("SIFT points: " << cloud_temp->size());
        if(cloud_temp->empty()) {
            ROS_WARN_STREAM("Cloud empty - no point detected");
            continue;
        }
        std::stringstream ss;
        ss << folder << "sift/" << f.filename().c_str();
        
        pcl::io::savePCDFile(ss.str(), *cloud_temp);

    }

    return 0;
}
