/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 04/27/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Visualize collected scans
 */

#include <ros/ros.h>
#include <laser_geometry/laser_geometry.h>
#include "CollectFnct.h"


bool update;
boost::mutex updateModelMutex;
pcl::PointCloud<pcl::PointXYZ>::Ptr pc_vis(new pcl::PointCloud<pcl::PointXYZ>);

void visualize() {
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(0, 0, 0);
    viewer->addCoordinateSystem(1.0, Eigen::Affine3f::Identity());
    viewer->initCameraParameters();

    while(!viewer->wasStopped() && ros::ok()) {
        viewer->spinOnce(50);
        boost::mutex::scoped_lock updateLock(updateModelMutex);
        if(update) {
            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> c1(pc_vis, 125, 125, 125);
            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> c2(pc_vis, 255, 0, 0);
            if(!viewer->updatePointCloud(pc_vis, c1, "sample cloud"))
                viewer->addPointCloud(pc_vis, c1, "sample cloud");
            update = false;
        }
        updateLock.unlock();
    }
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "visualize_collected_pc");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    boost::thread workerThread(visualize);

    CollectFnct cf;
    
    ros::Time t;
    t.fromSec(1398713484.060000);
    cf.print_time(t);
    return -1;
    fs::path path(std::string("") + getenv("HOME") + "/.ros/rfs_collect/scan");
    std::vector<fs::path> files;
    cf.get_all_files(path, files);

    std::string ts("2014-04-25 00:04:00.000");
    ros::Time start_time = ros::Time::fromBoost(boost::posix_time::time_from_string(ts));

    BOOST_FOREACH(fs::path & f, files) {
        ros::Time t = cf.extract_time(f.filename().c_str());
        if(t < start_time)
            continue;
        cf.print_time(t);

        sensor_msgs::LaserScan scan;
        cf.read_scan(scan, f.c_str());


        sensor_msgs::PointCloud2 spc;
        laser_geometry::LaserProjection projector;
        projector.projectLaser(scan, spc);

        pcl::PCLPointCloud2 pc;
        pcl_conversions::moveToPCL(spc, pc);
        boost::mutex::scoped_lock updateLock(updateModelMutex);
        pcl::fromPCLPointCloud2<pcl::PointXYZ>(pc, *pc_vis);
        update = true;
        updateLock.unlock();

        ROS_INFO_STREAM("Press enter for next scan");
        getchar();
        if(!ros::ok())
            break;
    }

    return 0;
}
