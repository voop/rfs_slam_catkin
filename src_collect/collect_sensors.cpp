/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 04/24/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Collect data from all sensors-
 *             Save data to the folder ~/.ros/
 */

#include <ros/ros.h>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <pcl/io/pcd_io.h>
#include <pcl_conversions/pcl_conversions.h>
#include <iostream>
#include <fstream>

class CollectSensors {
public:
    CollectSensors() : folder("/.ros/rfs_collect/"), node("~") {
        std::string path = getenv("HOME");
        path += "/.ros/rfs_collect/";
        boost::filesystem::create_directories(path + PC + "/");
        boost::filesystem::create_directories(path + SCAN + "/");
        boost::filesystem::create_directories(path + DEPTH + "/");
        period_depth = 0.5;
        period_pc = 0.5;
        period_scan = 0.5;

        ros::Time tnow = ros::Time::now();
        t_last_depth = boost::make_shared<ros::Time>(tnow);
        t_last_pc = boost::make_shared<ros::Time>(tnow);
        t_last_scan = boost::make_shared<ros::Time>(tnow);
    }

    void start() {
        ros::Subscriber sub;
        subs.resize(3, sub);
        subs[0] = node.subscribe("/xtion/depth/points", 1, &CollectSensors::cb_pc, this);
        subs[1] = node.subscribe("/scan", 1, &CollectSensors::cb_scan, this);
    }

    void stop() {
        BOOST_FOREACH(ros::Subscriber & sub, subs) {
            sub.shutdown();
        }
    }

    ~CollectSensors() {}


    void cb_depth() {
    }

    void cb_pc(const sensor_msgs::PointCloud2ConstPtr& rospc) {
        if(!should_measure(PC))
            return;
        std::string path = get_file_name(PC);
        pcl::io::savePCDFile(path, *rospc, Eigen::Vector4f(), Eigen::Quaternionf::Identity(), true);
        ROS_INFO_STREAM("Saved: " << path);
    }

    void cb_scan(const sensor_msgs::LaserScanConstPtr& msg) {
        if(!should_measure(SCAN))
            return;
        std::string path = get_file_name(SCAN);

        std::ofstream ofs(path.c_str(), std::ios::out | std::ios::binary);
        uint32_t serial_size = ros::serialization::serializationLength(*msg);
        boost::shared_array<uint8_t> obuffer(new uint8_t[serial_size]);
        ros::serialization::OStream ostream(obuffer.get(), serial_size);
        ros::serialization::serialize(ostream, *msg);
        ofs.write((char*) obuffer.get(), serial_size);
        ofs.close();
        ROS_INFO_STREAM("Saved: " << path);
    }

    /** \brief Decide whther should measure data for the sensro */
    bool should_measure(const std::string& sensor);

    /** \brief Get filename for the appropriate sensor.
     *  \param sensor 0 - PointCloud, 1 - LaserScan, 2 - DepthImage **/
    std::string get_file_name(const std::string& sensor);

    bool read_scan(sensor_msgs::LaserScan& msg, const std::string&  path);

public:
    const std::string folder;
    ros::NodeHandle node;
private:
    boost::shared_ptr<ros::Time> t_last_pc, t_last_scan, t_last_depth;
    double period_pc, period_scan, period_depth;
    static const std::string PC, DEPTH, SCAN;
    std::vector<ros::Subscriber> subs;
};

const std::string CollectSensors::PC = "pc";
const std::string CollectSensors::SCAN = "scan";
const std::string CollectSensors::DEPTH = "depth";




int main(int argc, char** argv) {
    ros::init(argc, argv, "collect_sensors");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(4);
    spinner.start();

    CollectSensors cs;
    cs.start();

    ros::waitForShutdown();
    spinner.stop();
    return 0;
}

bool CollectSensors::should_measure(const std::string& sensor)  {
    boost::shared_ptr<ros::Time> t;
    if(sensor == PC)
        t = t_last_pc;
    else if(sensor == SCAN)
        t = t_last_scan;
    else if(sensor == DEPTH)
        t = t_last_depth;

    ros::Time tnow = ros::Time::now();
    double tdiff = fabs(tnow.toSec() - t->toSec());
    if(tdiff < period_pc) {
        return false;
    }
    *t = tnow;
    return true;
}

std::string CollectSensors::get_file_name(const std::string& sensor)  {
    using namespace boost::posix_time;
    std::stringstream ss;
    time_facet* facet = new time_facet("%y-%m-%dT%H-%M-%S%F");
    ss.imbue(std::locale(ss.getloc(), facet));
    ss << getenv("HOME") << folder << sensor << "/";
    ss << "D" << ros::Time::now().toBoost();
    return ss.str();
}

bool CollectSensors::read_scan(sensor_msgs::LaserScan& msg, const std::string& path) {
    std::ifstream ifs(path.c_str(), std::ios::in | std::ios::binary);
    if(!ifs.is_open())
        return false;
    ifs.seekg(0, std::ios::end);
    std::streampos end = ifs.tellg();
    ifs.seekg(0, std::ios::beg);
    std::streampos begin = ifs.tellg();

    uint32_t file_size = end - begin;
    boost::shared_array<uint8_t> ibuffer(new uint8_t[file_size]);
    ifs.read((char*) ibuffer.get(), file_size);
    ros::serialization::IStream istream(ibuffer.get(), file_size);
    ros::serialization::deserialize(istream, msg);
    ifs.close();
    return true;
}
