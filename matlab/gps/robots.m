%clc;clear all;close all;
use_gps = false;
use_u = true;
robot = 1;

folder = '~/.ros/rfs_collect_robot1/';
if robot == 2
    folder = '~/.ros/rfs_collect_robot2/';
end

r1_start = 1398713491.244847 ;
r1_end = 1398713600.016070 ;
r2_start = 1398713600.516245 - 10;
r2_end = 1398713734.6095137 + 10;

r_start = r1_start;
r_end = r1_end;
if robot == 2
    r_start = r2_start;
    r_end = r2_end;
end
gps = dlmread([folder 'gps_not_angle.csv'],',')';
u = dlmread([folder 'inputs.csv'], ',')';

gps_start = find_closest_time(gps(1,:), r_start);
gps_end = find_closest_time(gps(1,:), r_end);
u_start = find_closest_time(u(1,:), r_start);
u_end = find_closest_time(u(1,:), r_end);

gps = gps(:,gps_start:gps_end);
u = u(:,u_start:u_end);

if use_u
    odom = compute_odometry(u);
    dlmwrite([folder 'slam_inputs.csv'], odom','precision',20);
elseif use_gps
    odom = compute_angle_gps(gps);
    dlmwrite([folder 'slam_inputs.csv'], odom','precision',20);
end

hold off
plot(gps(2,:), gps(3,:),'g')
hold on
plot(odom(2,:), odom(3,:),'r')
axis equal
