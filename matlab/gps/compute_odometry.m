function odom = compute_odometry(u)


odom = [];
x = 0; y = 0; theta = 0;
max_vel = 3.3; %maximum meters per sec
B = 0.52;
for i = 1:size(u,2)-1
    dt = abs(u(1,i) - u(1,i+1));
    vr = u(2,i) * max_vel;
    vl = -u(3,i) * max_vel;
    dr = vr * dt;
    dl = vl * dt;
    dtheta = (dr - dl) / (B * 2.0);
    delta = (dr + dl) / 2.0;
    
    x = x - delta * cos(theta + dtheta);
    y = y - delta * sin(theta + dtheta);
    theta = theta + dtheta;
    odom = [odom [u(1,i+1);x;y;theta]];
end
end

