function [i,c] = find_closest_time( times, time)
[c,i] = min(abs(times-time));
if(c > 0.5)
    disp('Closest time is more then a half second out')
end

end

