close all;
gps = dlmread(['/home/petrivl3/.ros/rfs_collect_night_1/gps_not_angle.csv'],',')';

plot(gps(2,:), gps(3,:), 'LineWidth', 2)
grid on
xlabel 'x [m]'
ylabel 'y [m]'
legend 'GPS'
save_pdf('','gps_total')