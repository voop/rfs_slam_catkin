%clc;clear all;close all;
use_gps = true;
use_u = false;
robot = 2;

folder = '~/.ros/rfs_collect_robot1/';
if robot == 2
    folder = '~/.ros/rfs_collect_robot2/';
end

avgp = dlmread([folder 'average_poses.csv'], ',')';
avgm = dlmread([folder 'maximum_poses.csv'], ',')';
r_start = avgp(1,1);
r_end = avgp(1,end);
gps = dlmread([folder 'gps_not_angle.csv'],',')';
u = dlmread([folder 'inputs.csv'], ',')';

gps_start = find_closest_time(gps(1,:), r_start);
gps_end = find_closest_time(gps(1,:), r_end);
u_start = find_closest_time(u(1,:), r_start);
u_end = find_closest_time(u(1,:), r_end);

gps = gps(:,gps_start:gps_end);
u = u(:,u_start:u_end);

odom = compute_odometry(u);
gps = compute_angle_gps(gps);

gps = transform_to_origin(gps);

hold off
plot(gps(2,:), gps(3,:),'g')
hold on
plot(odom(2,:), odom(3,:),'r')
plot(avgp(2,:), avgp(3,:),'-k')
plot(avgm(2,:), avgm(3,:),'--y')
axis equal
