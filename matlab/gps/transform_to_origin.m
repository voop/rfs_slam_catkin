function t = transform_to_origin(gps)

t = [];
T_start = T2D(gps(2:4,1));
for i = 1:size(gps,2)
    T = inv(T_start) *  T2D(gps(2:4,i));
    t = [t [gps(1,i);T2D_inv(T)]];
end

end

