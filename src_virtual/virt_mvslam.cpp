/**
 * Copyright (c) CVUT  - All Rights Reserved
 * Created on: 02/16/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *    Details: Virtual MVSLAM performance and precision analysis
 */

#include <ros/ros.h>
#include <rfs_slam/RosUtils.h>
#include <rfs_slam/EnvironmentSim.h>
#include "Particle.h"

using std::vector;
using std::string;
typedef std::pair<FPHD::vec_robot_state, FPHD::vec_vec_observation> RobotsInput;

void extract_robot_inputs(vector<RobotsInput>& robots_intputs,
                          FPHD::vec_vec_robot_state& trajectories, boost::shared_ptr<EnvironmentSim>& env,
                          double std_lin = 0.0, double std_ang = 0.0);

void extract_mvslam_robot_inputs(const vector<RobotsInput>& robots_intputs, int meeting_time,
                                 const Affine2d& meeting_transform, vector<RobotsInput>& robots_intputs_out);

void do_mvslam(double std_lin = 0.0, double std_ang = 0.0, int strategy = 0, bool save_partial = true) {
    std::stringstream ss;
    ss << "/tmp/virt_mvslam/map_" << std_lin << "_" << std_ang << "_" << strategy << "_";
    std::string folder = ss.str();

    vector<RobotsInput> robots_inputs, robots_inputs_fb;
    FPHD::vec_vec_robot_state trajectories;
    boost::shared_ptr<EnvironmentSim> env;
    extract_robot_inputs(robots_inputs, trajectories, env, std_lin, std_ang);

    int meeting_time = 27;
    Affine2d meeting_transform = trajectories[0][meeting_time].inverse() * trajectories[1][meeting_time];
    extract_mvslam_robot_inputs(robots_inputs, meeting_time, meeting_transform, robots_inputs_fb);

    Particle p;
    p.std_ang = std_ang;
    p.std_lin = std_lin;
    p.rss.push_back(trajectories[0][0]);
    ParticleFilter<Particle> pf(15625, p);

    if(save_partial) {
        env->save_features(folder + "features.csv");
        bool first = true;
        BOOST_FOREACH(const Affine2d & rs, trajectories[0]) {
            ROSUtils::save_pose(rs, folder + "pose1.csv", first);
            first = false;
        }
        first = true;
        BOOST_FOREACH(const Affine2d & rs, trajectories[1]) {
            ROSUtils::save_pose(rs, folder + "pose2.csv", first);
            first = false;
        }
    }
    bool first = true;
    ros::Time start = ros::Time::now();
    for(unsigned int t = 0; t < robots_inputs_fb.size(); ++t) {
        RobotsInput ri = robots_inputs_fb[t];

        if(save_partial) {
            // if(t % 10 == 9)
            ROS_INFO_STREAM(t);
        }

        if(t == meeting_time) { //add second and second-virtual robot state
            BOOST_FOREACH(Particle & p, pf.particles) {
                p.rss.push_back(p.rss[0] * meeting_transform);
                p.rss.push_back(p.rss[0] * meeting_transform);
            }
        }
        if(pf.particles.back().rss.size() != ri.first.size()) { //for removing virtual robots
            BOOST_FOREACH(Particle & p, pf.particles) {
                p.rss.resize(ri.first.size());
            }
        }

        if(strategy == 0 || strategy == 1) {
            for(int p_i = 0; p_i < pf.particles.size(); ++p_i) {
                Particle& p = pf.particles[p_i];
                p.motion(ri.first);
                double m_start = p.f.phd.sum();
		p.predict_update_prune(ri.second);
                double m_end = p.f.phd.sum();
                if(strategy == 0)
                    pf.weights[p_i] = Particle::compute_weight_empty_strategy(p, ri.second, m_start, m_end);
                if(strategy == 1)
                    pf.weights[p_i] = Particle::compute_weight_single_strategy(p, ri.second, m_start, m_end);
            }
            pf.normalize_weights();
            pf.resample();
        }

        if(strategy == 2) {
            for(int p_i = 0; p_i < pf.particles.size(); ++p_i) {
                Particle& p = pf.particles[p_i];
                p.motion(ri.first);
            }
            pf.set_weight_function(boost::bind(&Particle::compute_weight_phd_sum, _1, ri.second));
            pf.update_and_resample(false);
            #pragma omp parallel for
            for(int p_i = 0; p_i < pf.particles.size(); ++p_i) {
                Particle& p = pf.particles[p_i];
                //p.motion(ri.first);
                p.predict_update_prune(ri.second);
            }

        }

        if(save_partial) {
            FPHD::vec_robot_state poses;
            BOOST_FOREACH(const Particle & p, pf.particles) {
                BOOST_FOREACH(const Affine2d & rs, p.rss) {
                    poses.push_back(rs);
                }
            }
            ROSUtils::publish_phd(pf.get_maximum_weight_particle().f);
            ROSUtils::publish_poses(poses, pf.weights);
            ROSUtils::save_poses(poses, pf.weights, folder + "poses.csv", first);
            poses.clear();
            vector<double> weights;
            BOOST_FOREACH(const Affine2d & rs, ri.first) {
                poses.push_back(rs);
                weights.push_back(0.0);
            }
            ROSUtils::save_poses(poses, weights, folder + "inputs.csv", first);
        }
        first = false;
    }
    ros::Time end = ros::Time::now();
    double elapsed_time = end.toSec() - start.toSec();
    std::ofstream file;
    file.open((folder + "peformance.csv").c_str());
    file << elapsed_time << std::endl;
    file.close();
    ROS_INFO_STREAM(std_lin << "-" << std_ang << "-" << strategy << " : " << elapsed_time);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "fb_slam_sim");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool save_partial = true;
    node.param("save_partial", save_partial, true);
    ROS_INFO_STREAM("Save partial results: " << save_partial);

    boost::filesystem::path path("/tmp/virt_mvslam");
    boost::filesystem::create_directories(path);

    vector<double> std_lins;
//     std_lins.push_back(0.01);
    std_lins.push_back(0.02);
    vector<double> std_angs;
//     std_angs.push_back(0.00);
//     std_angs.push_back(0.01);
    std_angs.push_back(0.02);
    for(int strategy = 0; strategy < 2; strategy++) {
        BOOST_FOREACH(double std_lin, std_lins) {
            BOOST_FOREACH(double std_ang, std_angs) {
                do_mvslam(std_lin, std_ang, strategy, save_partial);
            }
        }
    }
    spinner.stop();
    return 0;
}

void extract_mvslam_robot_inputs(const vector< RobotsInput >& robots_intputs, int meeting_time, const Affine2d& meeting_transform, vector< RobotsInput >& robots_intputs_out) {
    int backward_time = -100;
    for(unsigned int t = 0; t < robots_intputs.size(); ++t) {
        RobotsInput ri = robots_intputs[t];
        if(t < meeting_time) { //only the first robot observations were received
            ri.first.erase(ri.first.begin() + 1); //erase second robot
            ri.second.erase(ri.second.begin() + 1);
        }
        if(t >= meeting_time) {
            if(backward_time == -100) {
                backward_time = t - 1;
                ri.first.push_back(Affine2d::Identity());
                ri.second.push_back(robots_intputs[backward_time].second[1]); //duplicate second robot
            } else {
                if(backward_time > 0) {
                    ri.first.push_back(robots_intputs[backward_time].first[1].inverse());
                    ri.second.push_back(robots_intputs[backward_time - 1].second[1]); //duplicate second robot
                }
                if(backward_time >= 0)
                    backward_time--;
            }
        }
        robots_intputs_out.push_back(ri);
    }
}

void extract_robot_inputs(vector< RobotsInput >& robots_intputs, FilterPHD< Vector2d, Matrix2d, Affine2d >::vec_vec_robot_state& trajectories, boost::shared_ptr< EnvironmentSim >& env, double std_lin, double std_ang) {
    env.reset(new EnvironmentSim(50, Vector3d(-10, -10, 0), Vector3d(10, 10, 0)));
    env->fov_distance = 4.0;
    env->fov_distance_min = 0.0;
    env->fov_H_angle = 0.5 * 270.0 / 180 * M_PI;
    env->fov_V_angle = 0.5 * 270.0 / 180 * M_PI;
    env->noise_measurement = 0.00;
    env->lambda_c = 0.00;
    trajectories.resize(2);

    for(int i = 0; i < 3; ++i) {
        const double ang_off_1(M_PI + M_PI / 8), ang_off_2(M_PI / 8);
        env->generate_circle_trajectory(4.0, Vector2d(5, 0), 20, trajectories[0]);
        env->generate_circle_trajectory(3.0, Vector2d(-5, 0), 20, trajectories[1], M_PI);
    }

    boost::mt19937 rng(2);
    boost::normal_distribution<> dist_lin(0.0, std_lin);
    boost::normal_distribution<> dist_ang(0.0, std_ang);
    for(unsigned int i = 1; i < trajectories[0].size(); ++i) {
        RobotsInput ri;
        BOOST_FOREACH(const FPHD::vec_robot_state & traj, trajectories) {
            Affine2d u = traj[i - 1].inverse() * traj[i];
            u.translation()[0] += dist_lin(rng);
            //u.translation()[1] += dist_lin(rng);
            Rotation2Dd rot(0.0);
            rot.fromRotationMatrix(u.linear());
            u.linear() = Rotation2Dd(rot.angle() + dist_ang(rng)).toRotationMatrix();
            ri.first.push_back(u);
            FPHD::vec_observation obsv;
            env->get_observation(obsv, traj[i]);
            ri.second.push_back(obsv);
        }
        robots_intputs.push_back(ri);
    }
}
